<?php

namespace Law\AdminBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Law\AdminBundle\Entity\Gallery;

class IllustrationRepository extends EntityRepository{
    
    public function findGalleryOrderedIllustrations(Gallery $gallery){

        
        $this->getEntityManager()->getRepository('NeostatDiagnosticoBundle:Diagnostico')->find($id);
        
        $query = $this->repo->createQueryBuilder('p')
                        ->where('p.id < :currentId')
                        ->andWhere('p.visible = 1')
                        ->setParameter('currentId', $gallery->getId())
                        ->orderBy('p.id', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery();        
        
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Product p ORDER BY p.name ASC'
            )
            ->getResult();        
        
    }
}
