<?php

namespace Law\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Section
 *
 * @ORM\Table(name="section", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */
class Section
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="galsCount", type="integer", nullable=false)
     */
    private $galscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="firstGallery", type="integer", nullable=true)
     */
    private $firstgallery;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set name
     *
     * @param string $name
     * @return Section
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set galscount
     *
     * @param integer $galscount
     * @return Section
     */
    public function setGalscount($galscount)
    {
        $this->galscount = $galscount;

        return $this;
    }

    /**
     * Get galscount
     *
     * @return integer 
     */
    public function getGalscount()
    {
        return $this->galscount;
    }

    /**
     * Set firstgallery
     *
     * @param integer $firstgallery
     * @return Section
     */
    public function setFirstgallery($firstgallery)
    {
        $this->firstgallery = $firstgallery;

        return $this;
    }

    /**
     * Get firstgallery
     *
     * @return integer 
     */
    public function getFirstgallery()
    {
        return $this->firstgallery;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
