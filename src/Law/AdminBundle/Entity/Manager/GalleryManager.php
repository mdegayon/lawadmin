<?php

namespace Law\AdminBundle\Entity\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use DoctrineORMEntityRepository;

use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Component\Helper\SlugUpdateHelper;
use Law\AdminBundle\Component\Collection\OrderedCollectionManager;

/**
 * Description of GalleryManager
 *
 * @author mdegayon
 */
class GalleryManager {

    protected $doctrine;

    /**
     * Entity-specific repo, useful for finding entities, for example
     * @var DoctrineORMEntityRepository
     */
    protected $repo;

    /**
     * The Fully-Qualified Class Name for our entity
     * @var string
     */
    protected $class;
    
    /**
     *
     * @var SlugUpdateHelper
     */
    protected $slugUpdateHelper;
    
    /**
     * Last Doctrine error
     * @var string
     */
    protected $lastError;
    
    /*
     * @var OrderedCollectionManager 
     */
    protected $orderedCollectionManager;

    public function __construct(Registry $doctrine, 
                                $class, 
                                SlugUpdateHelper $slugUpdateHelper, 
                                OrderedCollectionManager $orderedCollectionManager) {

        $this->doctrine = $doctrine;
        $this->class = $class;
        $this->repo = $doctrine->getRepository($class);
        $this->slugUpdateHelper = $slugUpdateHelper;
        $this->orderedCollectionManager = $orderedCollectionManager;

    }
    
    public function findGallery( $galleryId ){
                
        try{

            $gallery = $this->repo->find($galleryId);

        }catch(\Exception $e){

            $gallery = NULL;
            $this->lastError = $e->getMessage();
        }

        return $gallery;
    }
    
    public function getLastError(){
        
        return $this->lastError;
    }
    
    public function findPreviousGallery(Gallery $gallery){
                        
        $query = $this->repo->createQueryBuilder('p')
                        ->where('p.id < :currentId')
                        ->andWhere('p.visible = 1')
                        ->setParameter('currentId', $gallery->getId())
                        ->orderBy('p.id', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery();
 
        $previousGallery = $query->getOneOrNullResult();       
        
        if( !$previousGallery ){

            $galleries = $this->repo->findBy(
                array( 'visible' => 1 ), 
                array( 'id' => 'DESC'  ),
                1
            );
            $previousGallery = $galleries[0];
        }        
        
        return $previousGallery;
    }
    
    public function findNextGallery(Gallery $gallery){

        $query = $this->repo->createQueryBuilder('p')
                        ->where('p.id > :currentId')
                        ->andWhere('p.visible = 1')
                        ->setParameter('currentId', $gallery->getId())
                        ->orderBy('p.id', 'ASC')
                        ->setMaxResults(1)
                        ->getQuery();

        $nextGallery = $query->getOneOrNullResult();

        if( !$nextGallery ){

            $galleries = $this->repo->findBy(
                array( 'visible' => 1 ), 
                array( 'id' => 'ASC'  ),
                1
            );
            $nextGallery = $galleries[0];
        }        

        return $nextGallery;        
    }
    
    public function update(Gallery $gallery){

        $successfullUpdate = TRUE;

        $em = $this->doctrine->getManager();

        try{

            $this->slugUpdateHelper->handleSlugUpdate($gallery);

            $em->persist($gallery);
            $em->flush();

        }catch(\Exception $e){

            $successfullUpdate = FALSE;
            $this->lastError = (string) $e->getMessage();
        }

        return $successfullUpdate;
    }

    public function create(Gallery $gallery){

        $successfullInsert = TRUE;

        $em = $this->doctrine->getManager();
        
        //Insert as last gallery 
        $gallery->setOrder( $this->count() );

        try{

            $em->persist($gallery);
            $em->flush();           

        }catch(\Exception $e){

            $successfullInsert = FALSE;
            $this->lastError = (string) $e->getMessage();
        }

        return $successfullInsert;

    }

    public function delete(Gallery $gallery){

        $successfullDelete = TRUE;

        $em = $this->doctrine->getManager();        

        try{

            $em->remove($gallery);
            $em->flush();
            
            //Update galleries order after delete
            $this->orderedCollectionManager
                ->updateCollectionOrder($this->getGalleriesOrder());

        }catch(\Exception $e){

            $successfullDelete = FALSE;
            $this->lastError = (string) $e->getMessage();
        }

        return $successfullDelete;        
        
    }
    
    public function getGalleriesOrder(){

        return $this->doctrine
            ->getManager()
            ->createQuery('SELECT a.id FROM LawAdminBundle:Gallery a ORDER BY a.order')
            ->getResult("COLUMN_HYDRATOR");
    }
    
    public function count(){
        
        return $this->doctrine
            ->getManager()
            ->createQuery('SELECT COUNT(a.id) FROM LawAdminBundle:Gallery a')
            ->getSingleScalarResult();
    }

}
