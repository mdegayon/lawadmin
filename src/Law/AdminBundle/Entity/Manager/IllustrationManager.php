<?php

namespace Law\AdminBundle\Entity\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use DoctrineORMEntityRepository;
use Law\AdminBundle\Entity\Illustration;
use Law\AdminBundle\Entity\Gallery;

use Law\AdminBundle\Component\Collection\OrderedCollectionManager;

/**
 * Description of IllustrationManager
 *
 * @author mdegayon
 */
class IllustrationManager {
    
    protected $doctrine;

    /**
     * Entity-specific repo, useful for finding entities, for example
     * @var DoctrineORMEntityRepository
     */
    protected $repo;

    /**
     * The Fully-Qualified Class Name for our entity
     * @var string
     */
    protected $class;
    
    /*
     * @var OrderedCollectionManager 
     */
    protected $orderedCollectionManager;    
    
    /**
     * Last Doctrine error
     * @var string
     */
    protected $lastError;

    public function __construct( Registry $doctrine, $class, OrderedCollectionManager $orderedCollectionManager) {
          
        $this->doctrine = $doctrine;
        $this->class = $class;
        $this->repo = $doctrine->getRepository($class);
        $this->orderedCollectionManager = $orderedCollectionManager;

    }
    
    public function create(Illustration $illustration){
        
        //Insert as last gallery 
        $illustration->setOrder( 
            $this->countIllustrationsInGallery( $illustration->getGallery() ) 
        );
        
        $em = $this->doctrine->getManager();

        try{

            $em->persist($illustration);
            $em->flush();

        }catch(\Exception $e){

            $illustration = NULL;
            $this->lastError = $e->getMessage();
        }

        return $illustration;
        
    }
    
    public function delete(Illustration $illustration){

//        $galleryId = $illustration->getGallery()->getId();
//        var_dump( $this->getIllustrationsOrderInGallery($galleryId) );
//        die();
        
        $successfullDelete = TRUE;

        $em = $this->doctrine->getManager();        

        try{

            $galleryId = $illustration->getGallery()->getId();

            $em->remove($illustration);
            $em->flush();

            //Update galleries order after delete
            $this->orderedCollectionManager
                ->updateCollectionOrder(
                    $this->getIllustrationsOrderInGallery($galleryId)
                );

        }catch(\Exception $e){

            $successfullDelete = FALSE;
            $this->lastError = (string) $e->getMessage();
        }

        return $successfullDelete;        
        
    }
    
    public function getIllustrationsOrderInGallery($galleryId){

        return $this->doctrine
            ->getManager()
            ->createQuery(
                "SELECT "
                    . "a.id "
                . "FROM "
                    . "LawAdminBundle:Illustration a "
                . "WHERE "
                    . "a.gallery = :gallery "
                . "ORDER BY "
                    . "a.order ASC"
            )
            ->setParameters(
                array( 'gallery' => $galleryId )
            )
            ->getResult("COLUMN_HYDRATOR");
    }
    
    public function countIllustrationsInGallery(Gallery $gallery){

        return $this->doctrine
            ->getManager()
            ->createQuery(
                "SELECT "
                    . "COUNT(a.id) "
                . "FROM "
                    . "LawAdminBundle:Illustration a "
                . "WHERE "
                    . "a.gallery = :gallery"
            )
            ->setParameters(
                array( 'gallery' => $gallery->getId() )
            )
            ->getSingleScalarResult();
    }
    
    public function getLastError(){
        
        return $this->lastError;
    }    
    
}
