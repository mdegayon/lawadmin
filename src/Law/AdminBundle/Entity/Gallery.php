<?php

namespace Law\AdminBundle\Entity;

use Law\AdminBundle\Component\Collection\OrderedItem;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gallery
 *
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery implements OrderedItem {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, unique=true )
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Illustration", mappedBy="gallery")
     * @ORM\OrderBy({"order" = "ASC"})
     */
    protected $images;

    /**
     * @var string
     *
     * @ORM\Column(name="section", type="text", nullable=true)
     */
    private $section;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;


    /**
     * @var string
     *
     * @ORM\Column(name="bannerImage", type="text", nullable=true)
     */
    private $bannerImage;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnailImage", type="text", nullable=true)
     */
    private $thumbnailImage;

    /**
     * @var string
     *
     * @ORM\Column(name="descEn", type="text", nullable=true)
     */
    private $descen;

    /**
     * @var string
     *
     * @ORM\Column(name="descEs", type="text", nullable=true)
     */
    private $desces;

    /**
     * @var int
     *
     * @ORM\Column(name="visible", type="integer", nullable=true)
     */
    private $visible;

    public function __construct() {
        $this->images = new ArrayCollection();
    }

    function getSlug() {
        return $this->slug;
    }

    function getBannerImage() {
        return $this->bannerImage;
    }

    function getThumbnailImage() {
        return $this->thumbnailImage;
    }

    function setSlug($slug) {
        $this->slug = $slug;
    }

    function setBannerImage($bannerImage) {
        $this->bannerImage = $bannerImage;
    }

    function setThumbnailImage($thumbnailImage) {
        $this->thumbnailImage = $thumbnailImage;
    }

    function isVisible() {

        return $this->visible;
    }

    public function setVisible($visible) {

        $this->visible = $visible;
    }

    /**
     * Set section
     *
     * @param integer $section
     * @return Gallery
     */
    public function setSection($section) {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return integer 
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Gallery
     */
    public function setOrder($order) {

        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * Set firstillustration
     *
     * @param integer $firstillustration
     * @return Gallery
     */
    public function setFirstillustration($firstillustration) {
        $this->firstillustration = $firstillustration;

        return $this;
    }

    /**
     * Get firstillustration
     *
     * @return integer 
     */
    public function getFirstillustration() {
        return $this->firstillustration;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gallery
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set illustrationcount
     *
     * @param integer $illustrationcount
     * @return Gallery
     */
    public function setIllustrationcount($illustrationcount) {
        $this->illustrationcount = $illustrationcount;

        return $this;
    }

    /**
     * Get illustrationcount
     *
     * @return integer 
     */
    public function getIllustrationcount() {
        return $this->illustrationcount;
    }

    /**
     * Set descen
     *
     * @param string $descen
     * @return Gallery
     */
    public function setDescen($descen) {
        $this->descen = $descen;

        return $this;
    }

    /**
     * Get descen
     *
     * @return string 
     */
    public function getDescen() {
        return $this->descen;
    }

    /**
     * Set desces
     *
     * @param string $desces
     * @return Gallery
     */
    public function setDesces($desces) {
        $this->desces = $desces;

        return $this;
    }

    /**
     * Get desces
     *
     * @return string 
     */
    public function getDesces() {
        return $this->desces;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add images
     *
     * @param \Law\AdminBundle\Entity\Illustration $images
     * @return Gallery
     */
    public function addImage(\Law\AdminBundle\Entity\Illustration $images) {
        $images->setGallery($this);
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Law\AdminBundle\Entity\Illustration $images
     */
    public function removeImage(\Law\AdminBundle\Entity\Illustration $images) {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages() {

        return $this->images;
    }

}
