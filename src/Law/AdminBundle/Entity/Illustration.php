<?php

namespace Law\AdminBundle\Entity;

use Law\AdminBundle\Component\Collection\OrderedItem;

use Doctrine\ORM\Mapping as ORM;

/**
 * Illustration
 *
 * @ORM\Table(name="illustration")
 * @ORM\Entity
 */
class Illustration implements OrderedItem {

    /**
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="images")
     * @ORM\JoinColumn(name="gallery", referencedColumnName="id", onDelete="CASCADE")
     */
    private $gallery;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="text", nullable=false)
     */
    private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var int
     *
     * @ORM\Column(name="visible", type="integer", nullable=true)
     */
    private $visible = '1';    

    /**
     * Set gallery
     *
     * @param integer $gallery
     * @return Illustration
     */
    public function setGallery($gallery) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return integer 
     */
    public function getGallery() {
        return $this->gallery;
    }
    
    function isVisible() {

        return $this->visible;
    }

    public function setVisible($visible) {

        $this->visible = $visible;
    }    

    /**
     * Set next
     *
     * @param integer $next
     * @return Illustration
     */
    public function setOrder($order) {
        $this->order = $order;

        return $this;
    }

    /**
     * Get next
     *
     * @return integer 
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Illustration
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Illustration
     */
    public function setFile($file) {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

}
