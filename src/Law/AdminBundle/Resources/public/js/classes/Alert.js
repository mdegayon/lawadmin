function Alert(type, message){
    
    this.type = type;
    this.message = message;
    
    this.render = function(){
        
        return '<div class="alert alert-'+this.type+' alert-dismissible fade in" role="alert">'+
          '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
          this.message +
          '</div>';        
    };
    
}

