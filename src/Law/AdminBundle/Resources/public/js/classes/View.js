function View(DOMSelector, model, presenter) {

//    console.log("Create view");
//    console.log(model.properties);
//    console.log("Presenter's gallery");
//    console.log(presenter.gallery.properties);

    this.DOMSelector    = DOMSelector;
    this.model          = model;
    this.presenter      = presenter;

    this.setDOMSelector = function(DOMSelector){

        this.DOMSelector = DOMSelector;
    };

    this.setModel = function(model){

        this.model = model;
    };

    this.setPresenter = function(presenter){

        this.presenter = presenter;
    };

    this.update = function(){

//       console.log("Update view");
//       console.log(this.model.properties);
//       console.log(this.presenter.gallery.properties);

        this.DOMSelector.html(
            this.presenter.render()
        );

    };
}