function PorfolioManager(galleries) {
    
    this.galleries = new Array();
    
    this.setGalleries = function(galleries){
        
        this.galleries = galleries;
    };
    
    this.addGallery = function(gallery, view){

        console.log("Add gallery to array : ");
        console.log(view);

        gallery.addObserver(view);
        this.galleries.push(gallery);
    };
    
    this.deleteGallery = function(galleryId){
        
        console.log("Delete Gallery: " + galleryId);

        var selectedGallery = this.findGallery(galleryId);
        
        if(selectedGallery){

            var manager = new  PersistenceManager();
            var params = {
                'gallery' : selectedGallery,
                'manager' : this
            };
            manager.delete( selectedGallery, this.callbackDelete.bind(this), params);

        }

    };
    
    this.updateListOrder = function(){

        console.log('updateListOrder');

        var illustrations = [];

        $("#porfolio .galleryPanel").each(function( index, value ) {

            illustrations.push($(this).attr("data-law-param-id"));

        });

        var jsonOrder = JSON.stringify(illustrations);
        
        console.log(illustrations);
        console.log(jsonOrder);

        $.ajax({
            url: PORFOLIO_LIST_ORDER_POST_URI,
            type: 'POST',
            data: { order : jsonOrder},
            success: function(response) {
 
//                this.showAlert(alertType, alertMessage);

            }.bind(this)
        });

        console.log();
    };    
    
    this.toggleVisibility = function(galleryId, visible){

        console.log('toggleVisibility: '+galleryId);

        var selectedGallery = this.findGallery(galleryId);
        var manager = new PersistenceManager();

        if(selectedGallery){

            var galleryVisibility = visible  ? 1 : 0;
            selectedGallery.setVisible (galleryVisibility);

            //Save updates to DB
            manager.patch(
                galleryId,
                {'visible' : galleryVisibility}, 
                function(){}, 
                {}
            );
        }
    };

    this.callbackDelete = function(response, paramsBundle){
        
        console.log("CALLBACK DELETE");
        
        var manager = this,//paramsBundle.manager,
            gallery = paramsBundle.gallery;
    
        console.log(gallery);
        
        var alertMessage = 'Gallery "' + gallery.getName() + '" successfully deleted',
            alertType = 'success';        

        //If delete = ok
        if(response.result === 'ok'){
            
            console.log("Gallery successfully deleted");

            //Remove gallery from PorfolioManager array
            var galleryIndex = manager.galleries.indexOf(gallery);
            if (galleryIndex > -1) {
                manager.galleries.splice(galleryIndex, 1);
            }

            //Remove gallery from DOM
            $("[data-model-class='Gallery']").each(function( index ) {

                var currentId = $(this).find("[data-model-property='id']").html();

                console.log( currentId + " vs " + gallery.getId() );

                if( currentId == gallery.getId() ){

                    console.log("Fade out, motherfucker");
                    $(this).parent().fadeOut();
                }
            });

        //If some error happened, show error on alert's message
        }else{

            alertMessage = 'An error occurred while deleting gallery "' + 
                gallery.getName() + '" : <br />' + 
                response.error;

            alertType = 'danger';

        }
        //Show alert with result
        var alert = new Alert(alertType, alertMessage);
        $( "#alertContainer" ).prepend( alert.render() );
        $().alert();

    };
        
    this.getGalleryFilteredProperties = function(gallery){
      
        var filteredProperties = gallery.properties;
        
        delete filteredProperties['id'];
        
        return filteredProperties;
    };
    
    this.saveGallery = function(){

        alertType       = 'success';
        alertMessage    = 'Gallery successfully created';

        var parser = new Parser();
        var manager = new PersistenceManager();
        var porfolioManager = this;

        var gallery = parser.parseFormToObj( new Gallery(), $('#myModal') );

        var formData = new FormData(document.getElementById("newGalleryForm"));

            //Send form
            $.ajax({
                url: GALLERY_POST_URI,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {

                    if( response.result !== 'ok'){

                        alertType = 'danger';  
                        alertMessage = response.error;

                    }else{

                        $('#myModal').modal('hide');
                        this.appendNewGallery(response, gallery);
                    }

                    this.showAlert(alertType, alertMessage);               

                }.bind(this)
            });        
    };
    
    this.appendNewGallery = function(response, gallery){

        console.log('PorfolioManager::appendNewGallery');
        
        gallery.setId(response.id);
        
        //Correct thumbnail image path
        imageFakePath = gallery.getThumbnailImage();
        fakePathParts = imageFakePath.split('\\');
        gallery.setThumbnailImage(fakePathParts.pop().trim());
        
        gallery.setNew(false);
        
        var factory = new PresenterFactory(); 
        var presenter = factory.getPresenter(gallery);
        
        //Append gallery to document
        $( "div.porfolioContainer" ).append(
        
            '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc" style="margin-bottom: 1rem;">' +                
                '<div class="galleryPanel content-panel pn" data-model-class="Gallery">' + 
                
                    presenter.render() +
                    
                '</div>' +
            '</div>'
        );

        //Add new gallery to porfolio's galleries array     
        this.addGallery(
            gallery, 
            new View(
                $( "div.porfolioContainer [data-model-class='Gallery']" ).last(),
                gallery, 
                presenter
            )
        );
        
    };
    
    this.handleSaveResponse = function(response, gallery){

        if(response.result === 'ok'){
            
            var gritterTitle = '';
            var gritterText = '';
            /* New Gallery */            
            if(gallery.getId() === null){
                this.saveNewGallery(response, gallery);
                gritterTitle = 'Gallery created';
                gritterText  = 'The gallery was successfully created !';
            /* Already existant gallery */
            }else{                
                this.updateGallery(gallery);
                gritterTitle = 'Gallery updated';
                gritterText  = 'The gallery was successfully updated';
            }            
            $('#myModal').modal('hide');
            $.gritter.add({
                title: gritterTitle,
                text: gritterText
            });            

        }else{

            var alert = new Alert('danger', response.error);
            $( "#myModal .modal-body" ).prepend( alert.render() );
            $().alert();
        }
    };
    
    this.findGallery = function(galleryId){
        
        var requestedGallery = false;
        var i = 0;
        while (!requestedGallery && i < this.galleries.length) {
            
            if (this.galleries[i].properties['id'] == galleryId){
                requestedGallery = this.galleries[i];
            }
            i++;
        }
        return requestedGallery;
    }

    this.showAlert = function(alertType, alertMessage){

        //Show alert with result
        var alert = new Alert(alertType, alertMessage);
        
        $( "#alertContainer" ).prepend( alert.render() );
        $().alert();        
    };

}
