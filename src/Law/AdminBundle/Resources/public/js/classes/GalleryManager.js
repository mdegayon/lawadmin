function GalleryManager(gallery, illustrations, formId) {
    
    this.gallery = gallery;
    this.illustrations = illustrations;
    this.formId = formId;

    this.updateListOrder = function(){

        var illustrations = [];

        $("#illustrations .illustration").each(function( index, value ) {

            illustrations.push($(this).attr("data-law-param-id"));

        });

        var jsonOrder = JSON.stringify(illustrations);
        
        console.log(illustrations);
        console.log(jsonOrder);

        $.ajax({
            url: ILLUSTRATION_LIST_ORDER_POST_URI,
            type: 'POST',
            data: { order : jsonOrder},
            success: function(response) {

                console.log(response);

//                this.showAlert(alertType, alertMessage);

            }.bind(this)
        });

        console.log();
    };

    this.saveGallery = function(){
                        
        var parser = new Parser();
        var manager = new PersistenceManager();
        var galleryManager = this;
        
        alertType       = 'success';
        alertMessage    = 'Gallery successfully updated';       

        var parsedGallery = parser.parseFormToObj(
            this.filterGalleryFiles(this.gallery), 
            $('#galleryForm')
        );

        this.uploadGalleryImages('thumbnailImage', IMG_TYPE_THUMBNAIL);
        
        this.uploadGalleryImages('bannerImage', IMG_TYPE_BANNER);       

        //Save updates to DB
        manager.update(
                parsedGallery,
                function(response, callbackParamsBundle){

                    console.log("callback");
                    console.log(galleryManager);                                        

                    if( response.result !== 'ok'){
                                                
                        alertType = 'danger';  
                        alertMessage = response.error;                          
                    }
                    
                    this.showAlert(alertType, alertMessage);

                }.bind(this),
                {}
        );
    };

    this.uploadGalleryImages = function(name, type){

        if( $('input[type=file][name='+name+']').val() === '' ){
            return ;
        }

        alertType       = 'success';
        alertMessage    = 'Gallery\'s ' + name + ' successfully updated';

        var formData = new FormData();
        formData.append('imgType', type);
        formData.append('image', $('input[type=file][name='+name+']')[0].files[0]);

        //Send form
        $.ajax({
            url: GALLERY_IMG_POST_URI,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {

                if( response.result !== 'ok'){

                    alertType = 'danger';  
                    alertMessage = response.error;

                }else{

                    $('input[type=file][name='+name+']').val('');
                }

                this.showAlert(alertType, alertMessage);               

            }.bind(this)
        });
    };

    this.filterGalleryFiles = function(gallery){
        
        filteredGallery = new Gallery(gallery.properties);
        
        filteredGallery.setThumbnailImage('');
        filteredGallery.setBannerImage('');
        
        return filteredGallery;
        
    };

    this.setillustrations = function(illustrations){
        
        this.illustrations = illustrations;
    };
 
    this.uploadIllustrationCB = function(response){
        
        if( response.result !== 'ok'){
            
            $.gritter.add({
                title: 'Upload error',
                text: 'Some error happened during file upload',
                 time: '2000'
            });             
            return;
        }
        var illustration = new Illustration(
            {'id'       : response.illustration.id,
            'gallery'   : this.gallery.properties.id,
            'next'      : 1,
            'name'      : response.illustration.name,  
            'file'      : response.illustration.file}                
        );
        
        console.log(response);
        console.log(illustration);
        
        var presenter = new IllustrationPresenter(illustration, this.gallery.properties.slug);
        $('#illustrations').append(presenter.render());

        this.addIllustration(illustration);

//        $(".fancybox").fancybox();
//        $('.switch')['bootstrapSwitch']();        
    };
 
    this.addIllustration = function(illustration){

        this.illustrations.push(illustration);
    };
           
    this.showAlert = function(alertType, alertMessage){

        //Show alert with result
        var alert = new Alert(alertType, alertMessage);
        
        $( "#alertContainer" ).prepend( alert.render() );
        $().alert();        
    };

    this.deleteIllustration = function(id, name){
       
        bootbox.confirm(
            "Are you sure you want to delete the illustration '"+name+"' ?", 
            function(result){ 

                if(result === true){
                    
                    var url = ILLUSTRATION_DELETE_URI;
                    
                    $.ajax({
                        url: url.replace("ILLUSTRATION_ID", id),
                        type: 'DELETE',
                        data: { 'id' : id},
                        success: function(response) {

                            var alertMessage = 'illustration successfully deleted',
                                alertType = 'success'; 

                            //If delete = ok
                            if(response.result === 'ok'){

                                console.log("Gallery successfully deleted");

                                //Remove gallery from PorfolioManager array: TODO

                                //Remove gallery from DOM
                                $("[data-model-class='Illustration'][data-law-param-id='"+id+"']").fadeOut();                        

                            //If some error happened, show error on alert's message
                            }else{

                                alertMessage = 'An error occurred while deleting gallery "' + 
                                    gallery.getName() + '" : <br />' + 
                                    response.error;

                                alertType = 'danger';

                            }
                            //Show alert with result
                            this.showAlert(alertType, alertMessage);

                        }.bind(this)
                    });
                }
            }
        );
    };

    this.toggleVisibility = function(id, visible){

        var url = ILLUSTRATION_PATCH_URI;

        $.ajax({
            url: url.replace("ILLUSTRATION_ID", id),
            type: 'PATCH',
            data: { 'id' : id, 'visible' : (visible  ? 1 : 0) },
            success: function(response) {

                //If something went wrong during update
                if(response.result !== 'ok'){

                    alertMessage = 'An error occurred while deleting gallery "' + 
                        gallery.getName() + '" : <br />' + 
                        response.error;

                    alertType = 'danger';
                    
                    //Show alert with result
                    this.showAlert(alertType, alertMessage);
                }

            }.bind(this)
        });        
        
    };

}


