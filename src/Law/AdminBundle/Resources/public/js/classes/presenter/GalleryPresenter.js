function GalleryPresenter(gallery) {
    
    this.gallery = gallery;
    
    this.setGallery = function(gallery){
        this.gallery = gallery;
    };
    this.getGallery = function(){
        return this.gallery;
    };
    
    this.render = function(){

        console.log("Gallery.render");
        console.log(this.gallery.properties);
        console.log(this.gallery.isVisible( ) );
        console.log("End gallery.render");

        var background = 'grey';
        if(this.gallery.getThumbnailImage()){
            background = "url(/lawAdmin/web/img/porfolio/"+this.gallery.getSlug()+"/thumb/"+this.gallery.getThumbnailImage()+") no-repeat center top";
        }
        
        var visibilityIcon = 'fa-eye';
        if( ! this.gallery.isVisible( ) ){
            visibilityIcon = 'fa-eye-slash';            
        }
        
        return  '<a  href="#"'+ 
                    'data-toggle="tooltip" '+ 
                    'data-placement="top" '+ 
                    'data-law-action="delete" '+ 
                    'data-law-param-id="'+this.gallery.getId()+'"'+ 
                    'data-law-param-name="'+this.gallery.getName()+'"'+ 
                    'title="Delete gallery"'+                       
                    'class="deleteGalleryButton btn btn-theme04 btn-sm">'+

                    '<i class="fa fa-times"></i>'+

                '</a>'+                    

                '<div class="visibilityContainer" '+                     
                     'data-law-param-id="'+this.gallery.getId()+'">'+

                        '<input id="visible_'+this.gallery.getId()+'" '+
                               'type="checkbox" '+
                               'data-law-action="toggleVisibility" '+
                               'data-law-param-id="'+this.gallery.getId()+'" '+
                               'data-model-property="visible" '+
                               'title="toggle visibility" '+                           
                               'class="tgl tgl-flip btn-theme04" />'+

                        '<label data-tg-off="&#xf070;"'+
                               'data-tg-on="&#xf06e;"'+ 
                               'for="visible_'+this.gallery.getId()+'"'+ 
                               'class="tgl-btn" >'+
                        '</label>'+                        
                '</div>'+
        
                '<span data-model-property="id" class="hidden">'+ this.gallery.getId() +'</span>'+
                '<span data-model-property="thumbnailImage" class="hidden">'+ this.gallery.getThumbnailImage() +'</span>'+                
                '<div id="profile-02" style="background: '+background+'">'+
                    '<div class="user">'+
                        '<h4 data-model-property="name">'+ this.gallery.getName() +'</h4>'+
                    '</div>'+
                '</div>'+
                '<div class="pr2-social centered">'+
                    '<a href="/lawAdmin/web/app_dev.php/admin/view/'+ this.gallery.getId()+ '" data-toggle="tooltip" data-placement="bottom" title="View gallery">'+
                        '<i class="fa fa-edit"></i>'+
                    '</a>'+
                '</div>';
    };
}
