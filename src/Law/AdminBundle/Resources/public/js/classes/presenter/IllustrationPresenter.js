function IllustrationPresenter(illustration, slug) {
    
    this.illustration   = illustration;
    this.slug           = slug;
    
    this.setIllustration = function(illustration){
        this.Illustration = illustration;
    };
    this.getIllustration = function(){
        return this.illustration;
    };
    
    this.render = function(){
        
        return  '<div   class="col-lg-4 col-md-4 col-sm-4 mb illustration"'+ 
                        'data-model-class="Illustration" '+
                        'data-law-param-id="'+this.illustration.getId()+'">'+

                    '<div class="content-panel pn">'+
   
                        '<a  href="#" '+
                            'data-toggle="tooltip" '+
                            'data-placement="top" '+
                            'data-law-action="delete" '+
                            'data-law-param-id="'+this.illustration.getId()+'" '+
                            'data-law-param-name="'+this.illustration.getFile().trim()+'" '+
                            'title="Delete illustration" '+
                            'class="deleteGalleryButton btn btn-theme04 btn-sm">'+

                          '<i class="fa fa-times"></i>'+

                        '</a>'+
      
                        '<div class="visibilityContainer" data-law-param-id="'+this.illustration.getId()+'">'+

                           '<input id="visible_'+this.illustration.getId()+'" '+
                                  'type="checkbox" '+
                                  'data-law-action="toggleVisibility" '+
                                  'data-law-param-id="'+this.illustration.getId()+'" '+
                                  'data-model-property="visible" '+
                                  'title="toggle visibility" '+
                                  'class="tgl tgl-flip btn-theme04" '+
                                  'checked="">'+

                           '<label data-tg-off="&#xf070;" '+
                                  'data-tg-on="&#xf06e;" '+
                                  'for="visible_'+this.illustration.getId()+'" '+
                                  'class="tgl-btn">'+
                           '</label>'+

                        '</div>'+

                        '<div  id="spotify" '+
                              'style="background: url(\'/img/porfolio/'+this.slug.trim()+'/illustrations/'+this.illustration.getFile().trim()+'\') no-repeat center top">'+
                        '</div>'+

                        '<p class="followers">'+
                          '<i class="fa fa-user"></i> '+this.illustration.getFile().trim() +
                        '</p>'+

                     '</div>'+

                  '</div>';

//        return  '<div class="col-lg-4 col-md-4 col-sm-4 mb illustration" '+
//                      'data-model-class="Illustration" '+
//                      'data-law-param-id="24">'+
//
//                    '<div class="content-panel pn">'+
//
//                        '<a href="#" '+
//                           'data-toggle="tooltip"'+
//                           'data-placement="top"'+
//                           'data-law-action="delete"'+
//                           'data-law-param-id="'+this.illustration.getId()+'" '+
//                           'data-law-param-name="'+this.illustration.getFile().trim()+'" '+
//                           'title="Delete gallery" '+
//                           'class="deleteGalleryButton btn btn-theme04 btn-sm">'+
//
//                            '<i class="fa fa-times"></i>'+
//
//                        '</a>'+
//
//                        '<div class="visibilityContainer"'+                     
//                             'data-law-param-id="'+this.illustration.getId()+'">'+
//
//                                '<input id="visible_'+this.illustration.getId()+'" '+ 
//                                       'type="checkbox" '+
//                                       'data-law-action="toggleVisibility" '+
//                                       'data-law-param-id="'+this.illustration.getId()+'" '+
//                                       'data-model-property="visible" '+
//                                       'title="toggle visibility" '+
//                                       'class="tgl tgl-flip btn-theme04" '+
//                                       'checked />'+
//
//                                '<label data-tg-off="&#xf070;" '+
//                                       'data-tg-on="&#xf06e;" '+
//                                       'for="visible_'+this.illustration.getId()+'" '+
//                                       'class="tgl-btn" >'+
//                                '</label>'+
//
//                        '</div>'+
//
//                        '<div id="spotify" style="background: url(\'/lawAdmin/web/img/porfolio/'+this.slug.trim()+'/illustrations/'+this.illustration.getFile().trim()+'\') no-repeat center top">'+
//                        '</div>'+
//                        '<p class="followers">'+
//                            '<i class="fa fa-user"></i>'+this.illustration.getFile().trim()+
//                        '</p>'+
//                    '</div>'+
//                '</div>';
    };
}
