function PresenterFactory() {   
    
    this.getPresenter = function(object){
        
        var presenter = null;
        
        switch(object.constructor.name) {
            case 'Gallery':
                presenter = new GalleryPresenter(object);
                break;
            default:                
        }        
        return presenter;
    };
}


