function Parser() {   
    
    String.prototype.capitalize = function() {
        return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
    };       
    
    this.parseObjtToForm = function(object, jqObject){
    
        //TODO Buscar tb la clase
        $.each(object.properties, function(index, value){

            jqObject.find("[data-model-property='"+index+"']").val(value);
        });
    };
    
    this.parseFormToObj = function(object, jqObject){
        
        var inputField; 
        var inputValue;
        
        $.each(object.properties, function(index, value){

            inputField = jqObject.find("[data-model-property='"+index+"']");
            
            switch(inputField.attr("type")) {
                
                case 'checkbox':
                case 'radio':
                    
                    inputValue = inputField.is(":checked") ? '1' : '0';
                    break;
                    
                case 'file':
                    inputValue = inputField[0].files[0];
                    

                default:
                    
                    inputValue = inputField.val();
                    break;
            }
            object.properties[index] = inputValue;
            
        });
        
        return object;
    };
    
    this.parseObjToHTML = function(object, jqObject){
        
        $.each(object.properties, function(index, value){

            jqObject.find("[data-model-property='"+index+"']").html(value);

        });
    };
    
    this.parseHTMLToObj = function(object, jqObject){

        var HTMLProperty;

        $.each(object.properties, function(index, value){

            if(value){
                value = value.trim();
            }

            HTMLProperty = jqObject.find("[data-model-property='"+index+"']").html();

            if(HTMLProperty){

                object.properties[index] = HTMLProperty.trim();
            }

            HTMLProperty = null;

        });

        return object;
    };

    this.parseGals = function(){
    
        var elements = Array();
        
        $(".porfolioContainer [data-model-class='Gallery']").each(function( index ) {
            
            var htmlGal = $(this);
            var currentGallery = new Gallery();
            
            $.each(currentGallery.properties, function(index, value){

//                console.log(index +" "+ htmlGal.find("[data-model-property='"+index+"']").html());
                currentGallery.properties[index] = htmlGal.find("[data-model-property='"+index+"']").html();
            });
//            console.log(currentGallery.properties);            
            elements.push(currentGallery);
        });

        return elements;
    };     
}
