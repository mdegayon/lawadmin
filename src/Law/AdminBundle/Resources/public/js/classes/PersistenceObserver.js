function PersistenceObserver(){
    
    this.persistenceManager = new PersistenceManager();    
    
    this.update = function(observable){
        
        console.log("Object to update : " + observable);
        console.log(observable);
        
        if(observable.isNew() ){
            //POST
            console.log("POST Object");
            var apiResponse  = this.persistenceManager.insert(observable);
        }else{
            //PUT
            console.log("PUT Object");
            var apiResponse  = this.persistenceManager.update(observable);
        }
        
        return apiResponse;
    };
}


