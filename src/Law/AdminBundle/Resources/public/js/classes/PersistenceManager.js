function PersistenceManager() {
    
    this.insert = function(observable, callback, callbackParamsBundle){

        console.log('INSERT');

        return this.apiCall( 
            GALLERY_POST_URI, 
            'POST', 
            observable.serializeProperties(),
            callback, 
            callbackParamsBundle
        );
    };
    
    this.update = function(observable, callback, callbackParamsBundle){
        
        console.log('UPDATE');
        
        return this.apiCall(
            GALLERY_PUT_URI, 
            'PUT', 
            observable.serializeProperties(),
            callback, 
            callbackParamsBundle
        );
    };
    
    this.delete = function(observable, callback, callbackParamsBundle){
        
        console.log('DELETE');
        
        var url = GALLERY_DELETE_URI;
        url = url.replace("GALLERY_ID", observable.properties.id);
                
        return this.apiCall(
            url, 
            'DELETE', 
            observable.serializeProperties(), 
            callback, 
            callbackParamsBundle
        );
    };
    
    this.find = function(id){
        
    };
    
    this.patch = function(id, updateBundle, callback, callbackParamsBundle){
        
        console.log('PARTIAL UPDATE');
        
        var url = GALLERY_PATCH_URI;
        url = url.replace("GALLERY_ID", id);        
        
        return this.apiCall(
            url, 
            'PATCH',
            JSON.stringify(updateBundle),
            callback,
            callbackParamsBundle
        );
    };    
    
    this.apiCall = function($url, method, data, callback, callbackParamsBundle){

        var apiReturn = '';
        
        $.ajax({
            url:            $url,
            type:           method,
            contentType:    'application/json',
            dataType:       'json',
            data:           data,
            success : function(rawResponse) {

                callback(rawResponse, callbackParamsBundle);
            },
            error : function(rawResponse) {

                callback(rawResponse, callbackParamsBundle);
            }
        });

//        return apiReturn;
    };    
    
    this.apiCallOld = function($url, method, data){

//        console.log('Data : ' + data);
        var apiReturn = '';
//        console.log("Async Call");
        $.ajax({
            url:            $url,
            type:           method,
            contentType:    'application/json',
            dataType:       'json',
            data:           data,
            async:          false,
            success : function(rawResponse) {
                
//                console.log('success');
                apiReturn = rawResponse;
//                console.log(apiReturn);
//                console.log('end raw');
//                apiReturn = JSON.parse(rawResponse);
//                console.log('end success');
            },
            error : function(rawResponse) {

//                console.log('error');
                apiReturn = rawResponse;
//                console.log(apiReturn);
//                apiReturn = JSON.parse(rawResponse);
//                console.log('end error');                
            }
        });
//        console.log("End async Call");
//        console.log("Result: " + apiReturn.result);
//        console.log("End of function");
        return apiReturn;
    };

}


