function MenuItem(idMenuItem, textFr, textEn, prix, supp, viewHtml, menu) {
    
    this.idItem = idMenuItem;
    this.textFr = textFr;
    this.textEn = textEn;
    this.prix = prix;
    this.supp = supp;
    this.viewHtml = viewHtml;
    this.menu = menu;
    this.NewForm = false;
    
    this.getId = function(){
    
    	return this.idItem;	    	
    };
    
    this.setId = function(idMenuItem){
    	
    	this.idItem = id;
    };
    
    this.getTextFr = function(){
    	
        return this.textFr;		    
    };
    
    this.getTextEn = function(){
    	
        return this.textEn;		    
    };
    
    this.getPrix = function(){
    	
        return this.prix;
    };    
    
    this.getSupp = function(){
    	
        return this.supp;		    
    };
    
    this.getViewHtml = function(){
    	
    	return this.getViewHtml; 
    };
    
    this.isNewForm = function(){
    	
        return this.NewForm;		    
    };
    
    this.setNewForm = function(newForm){
    	
        this.NewForm = newForm;		    
    };    
        
    this.removeForm = function(){
    	
    	if(this.viewHtml){    		
            $(this.viewHtml).fadeOut(1000, function() {
                $( this ).remove();
            });
    	}
    };
       
    this.getTableViewCode = function (){
		
        var tableView = '' ;

        tableView += '<span class="hiddenField itemId">';
        tableView +=  this.idItem;
        tableView +=  '</span>';			

        tableView += ' <div class="large-9 small-12 columns"><div class="large-12 small-12 columns"><div class="large-1 small-12 columns"><img class="rowFlagImg" src="/img/fr_flag.png"/></div><div class="large-11 small-12 columns"><p class="left frName">';	
        tableView +=  this.textFr;
        tableView += '</p></div></div><div class="large-12 small-12 columns"><div class="large-1 small-12 columns"><img class="rowFlagImg" src="/img/uk_flag.png"/></div><div class="large-11 small-12 columns"><p class="left enName">';
        tableView += this.textEn;
        tableView += ' </p></div></div><div class="large-12 small-12 columns"><div class="large-1 columns"></div><div class="large-11 small-12 columns prixNSupLabels" style="padding: 0px;"><div class="large-6 small-12 columns"><div class="left"><label>Prix</ label></div><p class="left"><span class="itemPrice">';
        if(this.prix === ''){
            tableView +='- ';
        }else{
            tableView +=this.prix;
        }
        tableView += ' </span>€ </p></div><div class="large-6 small-12 columns"><div class="left"><label>Supplément</ label></div><p class="left"><span class="itemSupp">';		 
        if(this.supp === ''){
            tableView +='- ';
        }else{
            tableView +=this.supp;
        }                
        tableView += ' </span>€</p></div></div></div></div><div class="large-3 small-12 columns"><div class="large-4 small-4 columns" align="center"><a class="editItemButton" ><i class="fi-page-edit action-icon" style="color: red;"></i></a></div><div class="large-4 small-4 columns" align="center"><a class="deleteItemButton"><i class="fi-trash action-icon"></i></a></div><div class="large-4 small-4 columns" align="center"><a class="setVisibleItemButton"><i class="fi-eye action-icon"></i></a></div></div>';				

        return tableView; 		
    };    
       
    this.getEditionViewCode = function () {

        var editionView = '';

        editionView += '<span class="hiddenField itemId">';
        editionView +=  this.idItem;
        editionView +=  '</span>';

        editionView += '<div class="large-12 small-12 columns"><div class="large-12 small-12 columns"><div class="large-1 small-12 columns">';
        editionView += '<img class="rowFlagImg" src="/img/fr_flag.png"/></div><div class="large-11 small-12 columns"><textarea id="formTextFr" placeholder="texte en français">';		
        editionView += this.textFr;
        editionView += '</textarea></div></div><div class="large-12 small-12 columns"><div class="large-1 small-12 columns"><img class="rowFlagImg" src="/img/uk_flag.png"/>';
        editionView += '</div><div class="large-11 small-12 columns"><textarea id="formTextEn" placeholder="texte en anglais">';
        editionView += this.textEn;
        editionView += '</textarea></div></div><div class="large-1 small-12 columns"></div><div class="large-11 small-12 columns"><div class="large-4 small-12 columns left">';
        editionView += '<div class="left"><label>Prix</ label></div><div class="left"><input type="text" id="formPrice" placeholder="prix du plat (€)" value="';
        editionView += this.prix;
        editionView += '" /></div></div><div class="large-5 small-12 columns left"><div class="left"><label>Supplément</ label></div><div class="left">';
        editionView += '<input type="text" id="formSupp" placeholder="supplément (€)" value="';
        editionView += this.supp;
        editionView += '" /></div></div><div class="large-3 small-12 columns"><div class="large-4 columns"></div><div class="large-4 columns"></div>';
        editionView += '<div class="large-4 small-12 columns" align="center"><a class="saveItem"><i class="fi-save action-icon"></i></a></div></div></div></div>';

        return editionView;				
    };       
        
    this.setTableMode = function() {

        var tableForm = this.getTableViewCode();

        $(this.viewHtml).html(tableForm);
        $( this.viewHtml ).removeClass( CLASS_FOR_ITEM_EDITION_NOPOINT ); 

        this.addTableListeners();
    	
    };
    
    this.showError = function(error){

        //Fermer les anciennes alertes
        $(this.viewHtml).find('.alert-box').fadeOut(1000, function() {
            $( this ).remove();
        });

        //Ajouter l'alerte aux élément du menu
        $(this.viewHtml).prepend( '<div data-alert class="alert-box alert"'+
            ' style="margin-left:2.0rem;margin-right:2.0rem;">'+error+
            '<a href="#" class="close">&times;</a></div>').hide().fadeIn("slow");
        
        //Fermer l'alerte après 3 seconds. 
        setTimeout(function(){

            $('.alert-box').fadeOut(500, function() {
                $( this ).remove();
            });
        }, 3000 );

        //Fermer aussi l'alert si l'utilisateur clique dessus
        $(this.viewHtml).find('.alert-box a').on('click', function(e){
            
            e.preventDefault();
            $(this).parent().fadeOut( 'slow', function() {
                $( this ).remove();
            });
        });
    };
    
    this.addTableListeners = function(){
    	
        var menuItem = this;

        $(menuItem.viewHtml).find(EDIT_ITEM).on('click', function(){

                menuItem.renderEditForm();
        });

        $(menuItem.viewHtml).find(DELETE_ITEM).on('click', function() {

                menuItem.showDeleteDialog();
        });

        $(menuItem.viewHtml).find(SET_VISIBLE_ITEM).on('click', function() {

                menuItem.setVisible();
        });
    };     
    
    this.addFormListeners = function(){
    	
        var menuItem = this;

        $(menuItem.viewHtml).find(SAVE_ITEM).on('click', function(){

                menuItem.saveItem();
        });
		    	
    };         
        
    this.saveItem = function(){
              
        var menuItem = this;
        $.ajax({
            
          type: "POST",          
          url: "restServices.php",
          data: { 
                op: SERVICE_SAVE_ITEM,
                id: this.idItem, 
                
                nom_fr: $(FORM_TEXT_FR).val(),
                nom_en: $(FORM_TEXT_EN).val(),
                prix: $(FORM_PRICE).val(),
                supp: $(FORM_SUPP).val(),
                menu_id: this.menu.menuId,
                cat_id: this.menu.displayedCat
            }
        }).done(function(msg){

            console.log('response: '+JSON.stringify(msg));

            if(msg.error === false){

//                console.log('OK');

                menuItem.idItem = msg.result;
                menuItem.textFr = $(FORM_TEXT_FR).val();
                menuItem.textEn = $(FORM_TEXT_EN).val();
                menuItem.prix = $(FORM_PRICE).val();
                menuItem.supp = $(FORM_SUPP).val();
                menuItem.NewForm = false;
                
                menuItem.setTableMode();
            }else{
                
//                console.log('KO');
                menuItem.showError(msg.error);
                //Traiter les erreurs
            }
          });       
    }
        
    this.setEditionMode = function() {
				
        var editionForm = this.getEditionViewCode();

        $(this.viewHtml).html(editionForm);
        $( this.viewHtml ).addClass( CLASS_FOR_ITEM_EDITION_NOPOINT );
        
        this.addFormListeners();
        this.menu.scrollToForm($( this.viewHtml ));
    };
    
    this.renderEditForm = function(){
    	
    	console.log('Edit');
    	
    	this.setEditionMode();
    	
    	this.menu.updateCurrentEditingItem(this);
    };
    
    this.showDeleteDialog = function(){

        var dialog = this.menu.getDialog();
        dialog.setMenuItem(this);
        dialog.show();        
    };
    
    this.deleteItem = function() {
    	
        console.log('Delete');        
        var menuItem = this;
        $.ajax({
            
          type: "POST",          
          url: "restServices.php",
          data: { 
                op: SERVICE_DELETE_ITEM,
                id: this.idItem, 
            }
        }).done(function(msg){

            console.log('response: '+JSON.stringify(msg));

            if(msg.error === false){
                menuItem.menu.deleteItemFromMenu(menuItem);
            }else{                
                menuItem.showError(msg.error);
            }
          });        
    };
    
    this.setVisible = function(){
    	
    	console.log('Visible');
        
    };
}