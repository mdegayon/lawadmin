function Gallery(properties) {
    
    this.properties = {    
        'id'                : (properties && properties.id ) ? properties.id : null,
        'section'           : (properties && properties.section ) ? properties.section : null,  
        'next'              : (properties && properties.next ) ? properties.next : null,  
        'firstIllustration' : (properties && properties.firstIllustration ) ? properties.firstIllustration : null,  
        'name'              : (properties && properties.name ) ? properties.name : null,  
        'slug'              : (properties && properties.slug ) ? properties.slug : null,  
        'illustrationCount' : (properties && properties.illustrationCount ) ? properties.illustrationCount : null,  
        'descen'            : (properties && properties.descen ) ? properties.descen : null,  
        'desces'            : (properties && properties.desces ) ? properties.desces : null,  
        'bannerImage'       : (properties && properties.bannerImage ) ? properties.bannerImage : null,  
        'thumbnailImage'    : (properties && properties.thumbnailImage ) ? properties.thumbnailImage : null,
        'visible'           : (properties && properties.visible ) ? properties.visible : null
    };
    this.name = 'gallery';
    this.observers = [];
    this.new = true;
        
    this.setName = function(name){
        this.properties.name = name;
    };
    this.getName = function(){
        return this.properties.name;
    };
    this.setId = function(id){
        this.properties.id = id;
    };    
    this.getId = function(){
        return this.properties['id'];
    };
    
    this.getThumbnailImage = function(){
        return this.properties.thumbnailImage;
    };
    
    this.setThumbnailImage = function(image){
        this.properties.thumbnailImage = image;
    };    
    
    this.getBannerImage = function(){
        return this.properties.bannerImage;
    };
        
    this.setThumbnailImage = function(thumbnailImage){
        
        this.properties.thumbnailImage = thumbnailImage;
    };

    this.setBannerImage = function(bannerImage){
        
        this.properties.bannerImage = bannerImage;
    };    
    
    this.getSlug = function(){
        return this.properties.slug;
    };

    this.isNew = function(){
        return this.new;
    };
    
    this.setNew = function(newGallery){

        this.new = newGallery;
    };
    
    this.isVisible = function(){
      
        return this.properties.visible === "1";
    };
    
    this.setVisible = function(visible){
      
        this.properties.visible = visible;
    };
    
    this.addObserver = function(observer){
      
        this.observers.push(observer);
    };
    
    this.removeObserver = function(observer){
        // TODO
    };
    
    this.saveState = function(){
        
        console.log("Gallery.saveState");
        var response = 'ok'; 
        for (i = 0; i < this.observers.length; i++) {
        
            console.log("Update Observer");            
            console.log(this.observers[i]);
            
            response = this.observers[i].update(this);
        }
        return response;
    };
    
    this.serializeProperties = function(){
                
        return  JSON.stringify(this.properties);
    };
    
    this.hydrateFromJQ = function(jQueryObject){                        
    };
    
    this.serializeModel = function(){

        var CapsName = this.name.substr(0,1).toUpperCase()+this.name.substr(1);
        var serializedObj = {};
        var fieldName = '';

        $.each(this.properties, function(index, value){

            fieldName = CapsName +'['+index+']';
            serializedObj[fieldName] = value;
        });

        return serializedObj;
    };
}