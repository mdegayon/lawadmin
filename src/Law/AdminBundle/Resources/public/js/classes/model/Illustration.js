function Illustration(properties) {
    
    this.properties = {    
        'id'                : (properties && properties.id ) ? properties.id : null,
        'gallery'           : (properties && properties.gallery ) ? properties.gallery : null,  
        'next'              : (properties && properties.next ) ? properties.next : null,  
        'name'              : (properties && properties.name ) ? properties.name : null,  
        'file'              : (properties && properties.file ) ? properties.file : null
    };
    this.name = 'illustration';
    this.observers = [];
    this.new = true;
        
    this.setName = function(name){
        this.properties.name = name;
    };
    this.getName = function(){
        return this.properties.name;
    };
    this.setId = function(id){
        this.properties.id = id;
    };    
    this.getId = function(){
        return this.properties.id;
    };    
    this.getFile = function(){
        return this.properties.file;
    };
    this.setFile = function(file){
        this.properties.file = file;
    };    

    this.isNew = function(){
        return this.new;
    };
    this.setNew = function(newGallery){

        this.new = newGallery;
    };
    
    this.addObserver = function(observer){
      
        this.observers.push(observer);
    };
    
    this.removeObserver = function(observer){
        // TODO
    };
    
    this.saveState = function(){
        
        console.log("Illustration.saveState");
        var response = 'ok'; 
        for (i = 0; i < this.observers.length; i++) {
        
            console.log("Update Observer");
            response = this.observers[i].update(this);
        }
        return response;
    };
    
    this.serializeProperties = function(){
                
        return  JSON.stringify(this.properties);
    };
    
    this.hydrateFromJQ = function(jQueryObject){                        
    };
    
    this.serializeModel = function(){

        var CapsName = this.name.substr(0,1).toUpperCase()+this.name.substr(1);
        var serializedObj = {};
        var fieldName = '';

        $.each(this.properties, function(index, value){

            fieldName = CapsName +'['+index+']';
            serializedObj[fieldName] = value;
        });

        return serializedObj;
    };
}
