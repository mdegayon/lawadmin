<?php

namespace Law\AdminBundle\Component;

use Law\AdminBundle\Entity\Gallery;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Description of FileUploadHelper
 *
 * @author mdegayon
 */
interface FileUploadHelper {

    public function updateGallery(Gallery $gallery, UploadedFile $file, $type);
}
