<?php

namespace Law\AdminBundle\Component;

use Law\AdminBundle\Component\Constants\PorfolioImageType;
use Law\AdminBundle\Entity\Gallery;

/**
 * Description of FilePathManager
 *
 * @author mdegayon
 */
class FilePathManager {
    
    private $base_folder,
            $porfolio_base_url,
            $pf_thumbnail_folder, 
            $pf_banner_folder, 
            $pf_illustration_folder;    
    
    public function __construct($rootDir,
                                $base_folder, 
                                $porfolio_base_url, 
                                $pf_thumbnail_folder, 
                                $pf_banner_folder, 
                                $pf_illustration_folder) {

        $this->base_folder              = $rootDir . '/..' . $base_folder;        
        $this->porfolio_base_url        = $porfolio_base_url;
        $this->pf_thumbnail_folder      = $pf_thumbnail_folder;
        $this->pf_banner_folder         = $pf_banner_folder;
        $this->pf_illustration_folder   = $pf_illustration_folder;

    }
    
    public function getFileFullName($fileName, $type, Gallery $gallery = null){

        return $this->getFilePath($type, $gallery) . $fileName;

    }
    
    public function getPorfolioPath(){

        return $this->base_folder;
    }
    
    public function getFilePath($type, Gallery $gallery = null){

        $filePath = '';

        switch ($type) {

            case PorfolioImageType::IMG_ILLUSTRATION:

                $filePath = $this->base_folder . $gallery->getSlug() . "/" . 
                            $this->pf_illustration_folder;
                break;

            case PorfolioImageType::IMG_PF_THUMBNAIL:
                $filePath = $this->base_folder . $gallery->getSlug() . "/" . 
                            $this->pf_thumbnail_folder;
                break;

            case PorfolioImageType::IMG_PF_BANNER:
                $filePath = $this->base_folder . $gallery->getSlug() . "/" . 
                            $this->pf_banner_folder;
                break;

            default:
                throw new InvalidArgumentException( $this->getUnsupportedFileTypeMessage($type) );

        }
        
        return $filePath;
    }
    
    public function getUrl($fileName, $type, Gallery $gallery = null){

        $url = '';

        switch ($type) {

            case PorfolioImageType::IMG_ILLUSTRATION:

                $url = $this->porfolio_base_url . $gallery->getSlug() . "/" . 
                            $this->pf_illustration_folder . $fileName;
                break;

            case PorfolioImageType::IMG_PF_THUMBNAIL:

                $url = $this->porfolio_base_url . $gallery->getSlug() . "/" . 
                            $this->pf_thumbnail_folder . $fileName;
                break;

            case PorfolioImageType::IMG_PF_BANNER:
                $url = $this->porfolio_base_url . $gallery->getSlug() . "/" . 
                            $this->pf_banner_folder . $fileName;
                break;

            default:
                throw new InvalidArgumentException
                    ( $this->getUnsupportedFileTypeMessage($type) );

        }

        return $url;
    }
    
    private function getUnsupportedFileTypeMessage($type){

        return                     
            "Unexpected image type ('$type'). "
            . "Supported types: " .
            "'" . PorfolioImageType::IMG_ILLUSTRATION ."', ".
            "'" . PorfolioImageType::IMG_PF_BANNER ."', ".
            "'" . PorfolioImageType::IMG_PF_THUMBNAIL ."', ";

    }

}
