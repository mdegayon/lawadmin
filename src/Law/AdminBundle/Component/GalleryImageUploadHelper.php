<?php

namespace Law\AdminBundle\Component;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Entity\Manager\GalleryManager;
use Law\AdminBundle\Component\FileUploadHelper;
use Law\AdminBundle\Component\Constants\PorfolioImageType;

/**
 * Description of GalleryImageUploadHelper
 *
 * @author mdegayon
 */
class GalleryImageUploadHelper implements FileUploadHelper {

    /**
     *
     * @var GalleryManager
     */
    private $galleryManager;

    public function __construct(GalleryManager $galleryManager) {

        $this->galleryManager = $galleryManager;

    }

    public function updateGallery(Gallery $gallery, UploadedFile $file, $type) {

        if( $type == PorfolioImageType::IMG_PF_BANNER ){

            $gallery->setBannerImage($file->getClientOriginalName());

        }elseif($type == PorfolioImageType::IMG_PF_THUMBNAIL){

            $gallery->setThumbnailImage($file->getClientOriginalName());

        }else{

            throw new \InvalidArgumentException("Unsupported image type");
        }

        return $this->galleryManager->update($gallery);
        
    }

}
