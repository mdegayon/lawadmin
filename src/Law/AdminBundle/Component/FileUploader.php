<?php

namespace Law\AdminBundle\Component;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Law\AdminBundle\Entity\Gallery;

/**
 * Description of FileUploader
 *
 * @author mdegayon
 */
class FileUploader {

    /**
     *
     * @var FilePathManager 
     */
    private $filePathManager;

    public function __construct($filePathManager) {

        $this->filePathManager = $filePathManager;
    }

    public function upload(UploadedFile $file, $type, Gallery $gallery) {

        //TODO : Usar como nombre del fichero
//        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
//        
//        $targetDir = $this->getPath($type, $gallery->getSlug());

        $targetDir = $this->filePathManager->getFilePath( $type, $gallery );
        
        $file->move($targetDir, $file->getClientOriginalName());

        return $file->getClientOriginalName();
//        return $fileName;
    }    

}
