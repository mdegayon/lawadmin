<?php

namespace Law\AdminBundle\Component\Constants;

/**
 * Description of PorfolioImageType
 *
 * @author mdegayon
 */
final class PorfolioImageType {

    const   IMG_ILLUSTRATION    = 'illustration',
            IMG_PF_THUMBNAIL    = 'thumbnail',
            IMG_PF_BANNER       = 'banner';
    
}
