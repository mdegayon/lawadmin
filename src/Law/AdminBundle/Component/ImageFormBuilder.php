<?php

namespace Law\AdminBundle\Component;

use Law\AdminBundle\Component\Constants\PorfolioImageType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImageFormBuilder
 *
 * @author mdegayon
 */
class ImageFormBuilder {

    private $formFactory;

    public function __construct($formFactory) {

        $this->formFactory = $formFactory;
    }

    public function buildForm(){

        return $this->formFactory->createBuilder(
            'form', 
            array(), 
            array(
                'csrf_protection'   => false,
                'allow_extra_fields' => TRUE
            )
        )
        ->add('image', 'file', array('required' => true))
        ->add('imgType', 'choice', array(
                'choices' => array(
                    PorfolioImageType::IMG_ILLUSTRATION => 'Illustration',
                    PorfolioImageType::IMG_PF_BANNER    => 'Banner',
                    PorfolioImageType::IMG_PF_THUMBNAIL => 'Thumbnail',
                ),
                'required'    => true,
                'empty_data'  => null
            )
        )->getForm();
        //return $this->container->get('form.factory')->createBuilder('form', $data, $options);        
    }
    
    
    
}
