<?php

namespace Law\AdminBundle\Component;

use Law\AdminBundle\Entity\Gallery;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Law\AdminBundle\Component\FileUploadHelper;
use Law\AdminBundle\Entity\Illustration;

/**
 * Description of IllustrationUploadHelper
 *
 * @author mdegayon
 */
class IllustrationUploadHelper implements FileUploadHelper{
    
    /**
     *
     * @var IllustrationManager
     */
    private $illustrationManager;
    
    public function __construct($illustrationManager) {

        $this->illustrationManager = $illustrationManager;

    }

    public function updateGallery(Gallery $gallery, UploadedFile $file, $type) {

        $uploadedIllustration = new Illustration();

        $uploadedIllustration->setGallery($gallery);
        $uploadedIllustration->setName($file->getClientOriginalName());
        $uploadedIllustration->setFile($file->getClientOriginalName());
        
        $illustration =  $this->illustrationManager->create($uploadedIllustration);
        
        if( $illustration === NULL){
            
            throw new \Exception($this->illustrationManager->getLastError() );
        }

        return $illustration;
    }

}
