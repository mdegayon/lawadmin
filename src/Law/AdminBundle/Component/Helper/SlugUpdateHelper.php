<?php

namespace Law\AdminBundle\Component\Helper;

use Doctrine\Bundle\DoctrineBundle\Registry;

use Symfony\Component\Filesystem\Filesystem;

use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Component\FilePathManager;

class SlugUpdateHelper {

    protected $doctrine;

    /**
     * Entity-specific repo, useful for finding entities, for example
     * @var DoctrineORMEntityRepository
     */
    protected $repo;

    /**
     *
     * @var FilePathManager
     */
    protected $filePathManager;
    
    public function __construct(Registry $doctrine, FilePathManager $filePathManager) {
          
        $this->doctrine = $doctrine;
        $this->repo = $doctrine->getRepository('LawAdminBundle:Gallery');
        
        $this->filePathManager = $filePathManager;
    }
    
    public function handleSlugUpdate(Gallery $gallery){
        
        $oldSlug = $this->getSlugFromDB($gallery);

        if(!$oldSlug){
            return;
        }
        
        if( $this->slugHasChanged($oldSlug, $gallery->getSlug() ) ){
            
            if( $this->slugAlreadyExists( $gallery->getSlug()) ){

                throw new \Exception("Slug '".$gallery->getSlug()."' already exists");
            }            

            $this->updateIllustrationsFolderName(
                $oldSlug, 
                $gallery->getSlug()
            );

        }
    }
    
    public function slugAlreadyExists($slug){
        
        return  count($this->repo->findBySlug($slug)) > 0 ;        
    }
    
    public function getSlugFromDB(Gallery $gallery){
        
        return $this->doctrine
            ->getManager()
            ->createQuery('SELECT a.slug FROM LawAdminBundle:Gallery a WHERE a.id = :id')
            ->setParameters( array( 'id' => $gallery->getId() ) )
            ->getSingleScalarResult();
    }
    
    public function slugHasChanged($oldSlug, $newSlug){
        
        return $oldSlug != $newSlug;
    }

    /**
     * 
     * @param type $oldSlug
     * @param type $newSlug
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     */
    public function updateIllustrationsFolderName($oldSlug, $newSlug){

        $porfolioFolder = $this->filePathManager->getPorfolioPath();
        $oldFolder      = $porfolioFolder . $oldSlug;
        $newFolder      = $porfolioFolder . $newSlug;

        $fs = new Filesystem();

        $fs->rename($oldFolder, $newFolder, TRUE);
    }    
    
}
