<?php

namespace Law\AdminBundle\Component\Factory;

use Law\AdminBundle\Component\IllustrationUploadHelper;
use Law\AdminBundle\Component\GalleryImageUploadHelper;

/**
 * Description of UploadHelperFactory
 *
 * @author mdegayon
 */
class UploadHelperFactory {
            
    public static function get($type){

        $instance = null;

        switch ($type) {

            case 'illustration':
                $instance = new IllustrationUploadHelper();
                break;

            case 'gallery':
                $instance = new GalleryImageUploadHelper();
                break;

            default:
                throw new InvalidArgumentException(
                    "Unsupported upload helper type ($type). "
                    . "Supported types: 'illustration', 'gallery'"
                );
        }

        return $instance;
    }

}
