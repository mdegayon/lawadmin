<?php

namespace Law\AdminBundle\Component\Collection;

interface OrderedItem {

    public function getOrder();
    public function setOrder($order);

}
