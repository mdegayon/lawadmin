<?php

namespace Law\AdminBundle\Component\Collection;

use Doctrine\Bundle\DoctrineBundle\Registry;

use Law\AdminBundle\Component\Collection\OrderedItem;

class OrderedCollectionManager {

    /**
     *
     * @var Registry Doctrine
     */
    protected $doctrine;

    /**
     * Entity-specific repo, useful for finding entities, for example
     * @var DoctrineORMEntityRepository
     */
    protected $repo;

    /**
     * The Fully-Qualified Class Name for our entity
     * @var string
     */
    protected $class;    
    

    public function __construct(Registry $doctrine, $class) {

        $this->doctrine = $doctrine;
        $this->class = $class;
        $this->repo = $doctrine->getRepository($class);
    }
    
    /**
     * 
     * @param array $collection 
     * @param array $order
     */    
    public function updateCollectionOrder(Array $orderList){
        
        for($i = 0; $i < sizeof($orderList); $i++){
            
            $currentItem = $this->findItem($orderList[$i]);
            
            $currentItem->setOrder($i);
            
            $this->updateItem($currentItem);

        }
    }
    
    public function updateItem(OrderedItem $item){
                
        $em = $this->doctrine->getManager();
        
        $em->persist($item); 
        $em->flush();
    }
    
    /**
     * 
     * @param Integer $id
     * @return OrderedItem
     */
    public function findItem($id){
        
        return $this->repo->find($id);
                
    }
    
}
