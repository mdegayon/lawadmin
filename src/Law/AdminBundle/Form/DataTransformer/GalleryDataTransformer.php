<?php

namespace Law\AdminBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class GalleryDataTransformer implements DataTransformerInterface {

    private $manager;

    public function __construct(ObjectManager $manager) {
        $this->manager = $manager;
    }

    /**
     * Transforms an object (gallery) to a string (number).
     *
     * @param  Gallery|null $gallery
     * @return string
     */
    public function reverseTransform(Gallery $gallery) {

        if (null === $gallery) {
            return '';
        }

        return $gallery->getId();
    }

    public function transform($id) {

        if (!$id) {

            throw new TransformationFailedException(sprintf(
                    'Missing id. Required for Gallery transformation', $id
            ));
        }

        $gallery = $this->manager
                ->getRepository('LawAdminBundle:Gallery')
                // query for the issue with this id
                ->find($id)
        ;

        if ($gallery === null) {

            throw new TransformationFailedException(sprintf(
                    'Couldn\'t find Gallery (id = %s)', $id
            ));
        }

        return $gallery;
    }

}
