<?php

namespace Law\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Law\AdminBundle\Form\DataTransformer\GalleryDataTransformer;

class IllustrationType extends AbstractType{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('name')
                ->add('next', NULL, array('required' => false))
                ->add('file', array('data_class' => null))
                ->add('gallery', NULL, array('invalid_message' => 'Invalid gallery id'));

        $builder->get('gallery')
            ->addModelTransformer(new GalleryDataTransformer($this->manager));

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver){

        $resolver->setDefaults(array(
            'data_class'        => 'Law\AdminBundle\Entity\Illustration',
            'csrf_protection'   => false,
        ));

    }
    
    /**
     * {@inheritdoc}
     */
    public function getName(){

        return 'illustration_form';
    }
}