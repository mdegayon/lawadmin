<?php

namespace Law\AdminBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UpdatedGalleryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $builder->setMethod("PUT");
        
        $builder->add('name', NULL, array('required' => false))
                ->add('slug', NULL, array('required' => false))
                ->add('section', NULL, array('required' => false))
                ->add('descen', NULL, array('required' => false))
                ->add('desces', NULL, array('required' => false))
                ->add('visible', NULL, array('required' => false));
    }
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'Law\AdminBundle\Entity\Gallery',
            'csrf_protection'   => false,
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}