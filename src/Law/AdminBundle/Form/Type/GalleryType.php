<?php

namespace Law\AdminBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GalleryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod("POST");

        $builder->add('name', NULL, array('required' => true) )
                ->add('slug', NULL, array('required' => true) )
                ->add('section', NULL, array('required' => false))
                ->add('descen', NULL, array('required' => false))
                ->add('thumbnailImage', 'file', array('data_class' => null, 'required' => false))
                ->add('bannerImage','file', array('data_class' => null, 'required' => false))
                ->add('desces', NULL, array('required' => false))
                ->add('visible', NULL, array('required' => false));
        
    } 
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array( 
            'data_class'        => 'Law\AdminBundle\Entity\Gallery',
            'csrf_protection'   => false,
        ));
    }        
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
//        return 'lawadmin_gallery';
    }
}