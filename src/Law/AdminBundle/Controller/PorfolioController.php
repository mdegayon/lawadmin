<?php

namespace Law\AdminBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;

class PorfolioController extends FOSRestController{

    private $response = array(
        'result'    => 'ok',
        'message'   => '',
        'error'     => '',
        'id'        => '',
    );

    /**
     * @Post("/porfolio/galleriesOrder/")
     */    
    public function postGalleriesOrderAction(){

        $orderArray = json_decode( $this->getRequest()->get('order'), true );        

        if (!empty($orderArray)) {

            /**
             * @var \Law\AdminBundle\Component\Collection\OrderedCollectionManager
             */
            $manager = $this->get('ordered_porfolio_manager');

            try{

                $manager->updateCollectionOrder($orderArray);

            }  catch (\Exception $e){

                $this->response['result'] = 'ko';
                $this->response['message'] = $e->getMessage();

            }

        }else{
            $this->response['result'] = 'ko';
            $this->response['message'] = 'Unable to handle the order request';
        }

        return $this->response;
    }

}
