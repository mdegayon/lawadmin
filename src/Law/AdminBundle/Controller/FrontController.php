<?php

namespace Law\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Law\AdminBundle\Component\Constants\PorfolioImageType;
use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Entity\User;

class FrontController extends Controller{

    /**
     * @Route("/", name="front_index")
     * @Template()
     */
    public function indexAction(){

//        $this->createAdminUser();

        //Get visible galleries
        $galleries = $this->getDoctrine()
            ->getRepository('LawAdminBundle:Gallery')->findBy(
                array( 'visible' => '1'),
                array('order' => 'ASC')
            );

        return array(
            'galleries' => $galleries,
        );
    }

    /**
     * @Route("/view/{slug}", name="front_gallery")
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"slug" : "slug"}})
     * @Template()
     */
    public function galleryAction(Gallery $gallery) {

        /**
         * @var Law\AdminBundle\Entity\Manager\GalleryManager Description
         */
        $galleryManager = $this->get('gallery_manager');             
        
        $illustrations = $this->getDoctrine()
            ->getRepository('LawAdminBundle:Illustration')->findBy(
                array( 
                    'visible' => '1', 
                    'gallery' => $gallery->getId() 
                ),
                array('order' => 'ASC')
            );
        
        return array(
            'gallery'           => $gallery,
            'illustrations'     => $illustrations,
            'previousGallery'   => $galleryManager->findPreviousGallery($gallery),
            'nextGallery'       => $galleryManager->findNextGallery($gallery),
            'porfolioImageType' => new PorfolioImageType()
        );
    }
    
    private function createAdminUser(){
 
        /**
         * @var FOS\UserBundle\Doctrine\UserManager FOSUserBundle user manager
         */
        $userManager = $this->get('fos_user.user_manager');

        /**
         * @var User 
         */
        $user = $userManager->createUser();

        $user->setUsername('lawra');
        $user->setEmail('laumade@hotmail.com');
        $user->setPlainPassword('yomismas');
        $user->setRoles(array('ROLE_SUPER_ADMIN'));
        $user->setEnabled(true);

        $userManager->updateUser($user);
    }

}