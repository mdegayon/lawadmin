<?php

namespace Law\AdminBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\Filesystem\Filesystem;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Entity\Illustration;
use Law\AdminBundle\Form\Type\IllustrationPartialUpdateType;
use Law\AdminBundle\Component\Constants\PorfolioImageType;

class IllustrationController extends FOSRestController{

    private $response = array(
        'result'    => 'ok',
        'message'   => '',
        'error'     => '',
        'id'        => '',
    );

    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * arrobaRoute("/{id}", requirements={"id" = "\d+","_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={"id" = "id"})
     * 
     */
    public function getIllustrationAction(Illustration $illustration){

        return $illustration;
    }

    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery")
     */
    public function postIllustrationAction(Gallery $gallery){  
        
        $form = $this->get('image_form_builder')->buildForm();        
        $form->submit($this->getRequest()->request->all());

        if ( !$form->isValid() ){

            return $this->buildErrorResponse((string) $form->getErrors(true, false));
        }

        /* 
         * Get file uploader service to move the uploaded file 
         * to the gallery's illustration folder
         */
        $fileUploader = $this->get('file_uploader');

        $fileName = $fileUploader->upload(
            $this->getRequest()->files->get('image'),
            PorfolioImageType::IMG_ILLUSTRATION, 
            $gallery
        );

        /* 
         * Get illustration upload helper service to complete upload. 
         * The service will create the new illustration entity and save it to the DB
         */
        $helper = $this->get('illustration_upload_helper');

        try{

            $updatedGallery = $helper->updateGallery(
                $gallery, 
                $this->getRequest()->files->get('image'), 
                PorfolioImageType::IMG_ILLUSTRATION
            );

        }  catch (\Exception $e){

            return $this->buildErrorResponse($e->getMessage());
        }

        if($updatedGallery === NULL ){

            return $this->buildErrorResponse( $helper->getLastError() );
        }

        $this->response['illustration'] = array(
            'id'    => $updatedGallery->getId(),
            'file'  => $fileName,
            'name'  => $fileName,
        );               
        return $this->response;
    }

    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * @ParamConverter("illustration", class="LawAdminBundle:Illustration")
     */
    public function patchIllustrationAction(Illustration $illustration){
        
        $form = $this->createForm( 
            new IllustrationPartialUpdateType(),
            $illustration,
            array( "method" => "PATCH", 'allow_extra_fields' => TRUE)
        );
        
        $form->submit($this->getRequest()->request->all(), false);

        if ( !$form->isValid() ){

            return $this->buildErrorResponse((string) $form->getErrors(true, false));
        }

        try{

            $em = $this->getDoctrine()->getManager();
            $em->persist($illustration);
            $em->flush();

            $this->response['id'] = $illustration->getId();

        }catch(\Exception $e){

            return $this->buildErrorResponse((string) $e->getMessage());
        }

        return $this->response;
    }
    
    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * @ParamConverter("illustration", class="LawAdminBundle:Illustration")
     */
    public function deleteIllustrationAction(Illustration $illustration){
        
        /* @var $filePathManager FilePathManager */
        $filePathManager = $this->get('file_path_manager');

        $illustrationFile = $filePathManager->getFileFullName(
            $illustration->getFile(), 
            PorfolioImageType::IMG_ILLUSTRATION, 
            $illustration->getGallery() 
        );

        $fs = new Filesystem();

        //Remove file
        $fs->remove( array($illustrationFile) );
        
        /* @var $manager \Law\AdminBundle\Entity\Manager\IllustrationManager */
        $manager = $this->get('illustration_manager');

        if( !$manager->delete($illustration) ){

            $this->buildErrorResponse( $manager->getLastError() );
        }

        return $this->response;
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {

        $errors = array();

        foreach ($form->getErrors() as $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

    private function buildErrorResponse($errorMessage)
    {
        $this->response['result']   = 'ko';
        $this->response['error']    = $errorMessage;
        $this->response['params']   = serialize( $this->getRequest()->request->all() );

        return $this->response;
    }

}
 