<?php

namespace Law\AdminBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Form\Type\GalleryType;
use Law\AdminBundle\Form\Type\PartialGalleryType;
use Law\AdminBundle\Form\Type\UpdatedGalleryType;
use Law\AdminBundle\Component\Constants\PorfolioImageType;
use Law\AdminBundle\Component\FileUploadHelper;


class GalleryController extends FOSRestController{

    private $response = array(
        'result'    => 'ok',
        'message'   => '',
        'error'     => '',
        'id'        => '',
    );

    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     * 
     */
    public function getGalleriesAction(Gallery $gallery){

        return $gallery;
    }

    /**
     * @Route(requirements={"_format"="json"})
     */
    public function postGalleriesAction(Request $request){

        $gallery = new Gallery();

        $form = $this->createForm(
            new GalleryType(), 
            $gallery, 
            array(  "method" => "post",  'allow_extra_fields' => TRUE )
        );

        $form->handleRequest($request);

        if ( ! $form->isValid()){

            $this->response['result'] = 'ko';
            $this->response['error']  = (string) $form->getErrors(true, false);

            return $this->response;
        }

        /* @var $galleryManager \Law\AdminBundle\Entity\Manager\GalleryManager */
        $galleryManager = $this->get('gallery_manager');        
        
        if ( $galleryManager->create($gallery) ){
            
            $this->uploadFile(
                $gallery, 
                PorfolioImageType::IMG_PF_THUMBNAIL,
                $request->files->get('thumbnailImage')
//                $this->getRequest()->files->get('thumbnailImage')
            );

            $this->uploadFile(
                $gallery, 
                PorfolioImageType::IMG_PF_BANNER,
                $request->files->get('bannerImage')
//                $this->getRequest()->files->get('bannerImage')
            );

            $this->response['id'] = $gallery->getId();

        }else{

            $this->response['result'] = 'ko';
            $this->response['error'] = $galleryManager->getLastError();
        }

        return $this->response;
    }

    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     */
    public function patchGalleriesAction(Request $request, Gallery $gallery)
    {
        $form = $this->createForm( 
            new PartialGalleryType(), $gallery, 
            array( "method" => "PATCH", 'allow_extra_fields' => TRUE)
        );

        $form->handleRequest($request->request->all(), false);
//        $form->submit($request->request->all(), false);
//        $form->submit($this->getRequest()->request->all(), false);

        if ( !$form->isValid() ){

            $this->response['result']   = 'ko';
            $this->response['error']    = (string) $form->getErrors(true, false);

            return $this->response;
        }

        try{

            $em = $this->getDoctrine()->getManager();
            $em->persist($gallery);
            $em->flush();

            $this->response['id'] = $gallery->getId();

        }catch(\Exception $e){

            $this->response['result']   = 'ko';
            $this->response['error']    = (string) $e->getMessage();
        }

        return $this->response;
    }    
    
    /**
     * @Route(requirements={"_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     */
    public function deleteGalleryAction(Gallery $gallery){
                
        //Remove gallery files and illustrations
        $fs = new Filesystem();

        /* @var Law\AdminBundle\Component\FilePathManager */
        $pathManager = $this->get('file_path_manager');        
        $fs->remove(array(
            $pathManager->getPorfolioPath() . $gallery->getSlug().'/'
        ));

        /* @var $galleryManager \Law\AdminBundle\Entity\Manager\GalleryManager */
        $galleryManager = $this->get('gallery_manager');
        
        if( !$galleryManager->delete($gallery) ){
            
            $this->response['result']   = 'ko';
            $this->response['error']    = $galleryManager->getLastError();
        }

        return $this->response;
    }

    /**
     * @Route(requirements={"_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     */    
    public function postGalleriesImageAction(Request $request, Gallery $gallery){

        $this->response['result']   = 'ko';
        $this->response['error']    = 'postGalleriesImageAction';
        return $this->response;
        
        //Successful default response
        $this->response = array('result' => 'ok',);

        $form = $this->get('image_form_builder')->buildForm();
        $form->handleRequest($request);
//        $form->submit($request);
//        $form->submit($this->getRequest()->request->all());

        if ( !$form->isValid() ){

            $this->response['result'] = 'ko';
            $this->response['error'] = (string) $form->getErrors(true, false);
            return $this->response;

        }elseif( $request->files->get('image') == null ){
//        }elseif( $this->getRequest()->files->get('image') == null ){

            $this->response['result'] = 'ko';
            $this->response['error'] = 'Missing uploaded file';
            return $this->response;
        }

        $data = $form->getData();

        $fileName = $this->uploadFile(
                $gallery, 
                $data['imgType'],
                $request->files->get('bannerImage')
//                $this->getRequest()->files->get('bannerImage')
            );

        return $this->response;
    }

    /**
     * @Route("/{id}", requirements={"_format"="json"})
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     */    
    public function putGalleriesAction(Request $request, Gallery $gallery){

        $form = $this->createForm(
            new UpdatedGalleryType(),
            $gallery,
            array( 
                "method" => "put", 
                'allow_extra_fields' => TRUE
            )   
        );
        
        $form->handleRequest($request);

        if (! $form->isValid()){

            $this->response['result'] = 'ko';
            $this->response['error'] =   (string) $form->getErrors(true, false);
        }

        /* @var $galleryManager \Law\AdminBundle\Entity\Manager\GalleryManager */
        $galleryManager = $this->get('gallery_manager');

        if ( $galleryManager->update($gallery) ){

            $this->response['id'] = $gallery->getId();

        }else{

            $this->response['result'] = 'ko';
            $this->response['error'] = $galleryManager->getLastError();
        }

        return $this->response;
    }
    
    /**
     * arrobaRoute("/{id}/illustrationsOrder/",  requirements={"_format"="json"})
     * @Post("/galleries/{id}/illustrationsOrder/")
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     */
    public function postGalleryIllustrationsOrderAction(Request $request, Gallery $gallery = null){

        if(!$gallery){
            
            $this->response['result'] = 'ko';
            $this->response['message'] = 'Unable to find the requested gallery';
            return $this->response;
        }

        $orderArray = json_decode($request->get('order'), true );
//        $orderArray = json_decode( $this->getRequest()->get('order'), true );        

        if (!empty($orderArray)) {

            /* @var $manager \Law\AdminBundle\Component\Collection\OrderedCollectionManager */
            $manager = $this->get('ordered_gallery_manager'); 

            try{
                
                $manager->updateCollectionOrder($orderArray);
                
            }  catch (\Exception $e){
                
                $this->response['result'] = 'ko';
                $this->response['message'] = $e->getMessage();

            }

        }else{
            $this->response['result'] = 'ko';
            $this->response['message'] = 'Unable to handle the order request';

        }

        return $this->response;
    }

    /**
     * 
     * @return FileUploadHelper
     */
    private function findUploadHelper($type){

        $helper = NULL;

        switch ($type) {

            case PorfolioImageType::IMG_ILLUSTRATION :
                
                $helper = $this->get('illustration_upload_helper');
                break;

            case PorfolioImageType::IMG_PF_BANNER :
            case PorfolioImageType::IMG_PF_THUMBNAIL :

                $helper = $this->get('gallery_image_upload_helper');
                break;

            default:
                throw new \InvalidArgumentException('Unsupported image type');
        }
        
        return $helper;
    }

    private function uploadFile(Gallery $gallery,  $type, UploadedFile $image = null ){

        if( $image === null){
            return '';
        }

        /* @var FileUploader */
        $fileUploader = $this->get('file_uploader');
        
        $fileName = $fileUploader->upload( $image,  $type, $gallery );

        $helper = $this->findUploadHelper( $type );
        $helper->updateGallery($gallery, $image, $type);        
        
        return $fileName;
    }

    private function getValidationErrorMessages(Gallery $gallery){
        
        // get a ConstraintViolationList
        $errors = $this->get('validator')->validate( $gallery );
        $result = '';

        foreach( $errors as $error ){
            // Do stuff with:
           echo $error->getPropertyPath() . PHP_EOL;
           echo $error->getMessage() . PHP_EOL;
        }   
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {

        $errors = array();

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

}
