<?php

namespace Law\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


use Doctrine\ORM\EntityManager;

use Law\AdminBundle\Entity\Gallery;
use Law\AdminBundle\Entity\Section;
//use Law\AdminBundle\Entity\Illustration;
use Law\AdminBundle\Component\Constants\PorfolioImageType;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="admin_index")
     * @Method({"GET"}) 
     */    
    public function indexAction(){

        $sections = $this->getDoctrine()
            ->getRepository('LawAdminBundle:Section')
            ->findAll();

        $galleries = $this->getDoctrine()
            ->getRepository('LawAdminBundle:Gallery')
            ->findBy(array(),array('order' => 'ASC'));

        return $this->render(
            "LawAdminBundle:Default:porfolio/porfolio.html.twig",
                array(
                    'name' => 'Law', 
                    'sections' => $sections, 
                    'galleries' => $galleries,
                )
        );
    }

    /**
     * @Route("/view/{id}", name="admin_gallery")
     * @ParamConverter("gallery", class="LawAdminBundle:Gallery", options={{"id" : "id"}})
     * @Method({"GET"})
     */
    public function viewGalleryAction(Gallery $gallery){
                                        
        $sections = $this->getDoctrine()
            ->getRepository('LawAdminBundle:Section')
            ->findAll();
  
        return $this->render(
            "LawAdminBundle:Default:gallery/gallery.html.twig",
                array(
                    'sections'  => $sections, 
                    'gallery'   => $gallery,
                    'images'    => $gallery->getImages(),
                    'img_illustration'  => PorfolioImageType::IMG_ILLUSTRATION,
                    'img_banner'        => PorfolioImageType::IMG_PF_BANNER,
                    'img_thumbnail'     => PorfolioImageType::IMG_PF_THUMBNAIL,
                )
        );
    }

}
