<?php

namespace Law\AdminBundle\Tests\Component;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Law\AdminBundle\Component\FilePathManager;

class FilePathManagerTest extends WebTestCase {

    private static          
        /**
         * @var FilePathManager Description
         */
        $filePathManager, 
        $container;
    
    public static function setUpBeforeClass(){
        
        //start the symfony kernel
        $kernel = static::createKernel();
        $kernel->boot();

        //get the DI container
        self::$container = $kernel->getContainer();

        //now we can instantiate our service (if you want a fresh one for
        //each test method, do this in setUp() instead
        self::$filePathManager = self::$container->get('file_path_manager');        
    }
    
    public function testPorfolioPath(){

        $porfolioPath =  self::$filePathManager->getPorfolioPath();

        $this->assertEquals(
            $porfolioPath, 
            "C:\wamp\www\lawAdmin\app/../web/img/porfolio/"
        );
    }    
    
}
