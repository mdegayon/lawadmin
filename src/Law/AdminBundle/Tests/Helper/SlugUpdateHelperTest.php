<?php


class SlugUpdateHelperTest {

    private static
        /**
         * @var FilePathManager Description
         */
        $slugUpdateHelper, 
        $container;
    
    public static function setUpBeforeClass(){
        
        //start the symfony kernel
        $kernel = static::createKernel(); 
        $kernel->boot();

        //get the DI container
        self::$container = $kernel->getContainer();

        //now we can instantiate our service (if you want a fresh one for
        //each test method, do this in setUp() instead
        self::$filePathManager = self::$container->get('slug_update_helper');
    }
    
    public function setUp(){

        $this->testsub = new TestSubject();
    }
    
    public function tearDown(){

        unset($this->testsub);
    }
    
}
