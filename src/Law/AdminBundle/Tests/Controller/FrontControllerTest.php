<?php

namespace Law\AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
    }

    public function testGallery()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/view');
    }

}
